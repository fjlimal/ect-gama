﻿module EctApp.Service {
   export class ErrorModalService {
      static $inject = ["$uibModal"];

      constructor(
         public uibModal: ng.ui.bootstrap.IModalService
      ) {

      }

      public openModal(title: string = 'padrão') {
         this.uibModal.open({
            templateUrl: 'Templates/popup.html',
            controller: 'ModalController as ctrl',
            resolve: {
               title: () => { return title }
            }
         });
      }
   }

   class ModalController {
      static $inject = ['title']
      constructor(
         public title: string
      ) { }
   }

   var module = getModule();
   module.service("ErrorModalService", ErrorModalService);
   module.controller('ModalController', ModalController);

}
