﻿module EctApp.Controllers {
   import services = EctApp.Service;
   export class MainContronller {
      static $inject = ["ErrorModalService"];
      public showIt: boolean = false;
      constructor(
         private errorModal:services.ErrorModalService
      ) {
      }

      public ShowIt(): void {
         this.showIt = !this.showIt;
         this.errorModal.openModal("error");
      }


   }
   var module = getModule();
   module.controller('MainContronller', MainContronller);

}