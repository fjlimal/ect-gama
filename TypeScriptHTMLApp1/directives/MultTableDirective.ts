﻿module EctApp.Directives {
   'use strict';

   export class MultTableDirective implements angular.IDirective {

      public link: (scope: IDirectiveScope, element: angular.IAugmentedJQuery, attrs: angular.IAttributes) => void;
      public templateUrl: string = 'Templates/directive.html';
      public scope:any = {"numero":"@"};
      public restrict: string = 'E';
      public replace = false;
      public controller: string = 'DirectiveController';
      public controllerAs: string = 'ctrl';
      static instance(): ng.IDirective {
         return new MultTableDirective();
      }

      constructor() { }

   }

   class DirectiveController{
   static $inject = ['$scope']
   constructor(
      public scope:IDirectiveScope
   ) { }
   }

   interface IDirectiveScope extends ng.IScope {
      numero: number;
   }



   var app = getModule();
   app.controller('DirectiveController', DirectiveController);
   app.directive('multTable',   MultTableDirective.instance );
}